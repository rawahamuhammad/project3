all:proj3

proj3: proj3_main.o proj3_functions.o 
	gcc -o proj3 proj3_main.o proj3_functions.o
	
proj3_main.o: proj3_main.c proj3_header.h
	gcc -c proj3_main.c
	
proj3_functions.o: proj3_functions.c proj3_header.h
	gcc -c proj3_functions.c
	
clean:
	rm ./*.o proj3
