/*
 * Name: Rawaha
 * SID: 9143
 * CID: 102309
 */

#include <stdio.h>
#include <stdlib.h>
#include "proj3_header.h"


int main(int argc, char* argv[])
{
    if(argc <= 1)
    {
        printf("Program will exit now, no valid argument was given\n");
    }
    else
    {
        if(atoi(argv[1])==1)
        {
            Redirect(argv,argc);
        }
        else if(atoi(argv[1]) == 2)
        {
            ShowWordCount();
        }
        else if(atoi(argv[1])== 3)
        {
            SigTest();
        }
        else
        {
            printf("Program will exit now, no valid argument was given\n");
        }
    }
}
