/*
 * Name: Rawaha
 * SID: 9143
 * CID: 102309
 */


#include "proj3_header.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>

/*Redirect
 * char* argv[]: the arg vector used to write text on File1.txt
 * int argc: the length of this vector
 *  
 * This method redirects the output from stout to a file, and then restores the output back to stdout
 * used: dup(), dup2(), creat() 
 */

void Redirect(char* argv[], int argc){
    int fileDesc = creat("./File1.txt",00700);
    int saved_stdout = dup(1);                      //redirect stdout to the first open file descriptor space
    
    fflush(stdout); 
    //comments
    dup2(fileDesc,STDOUT_FILENO);                   //write contents to this file descriptor
    for(int i = 2;i<argc;i++)
    {
        printf("%s\t",argv[i]);
    }
    
    fflush(stdout); 
                                    //flush the buffer
    dup2(saved_stdout,STDOUT_FILENO);               //redirect stdout back to the saved fd of dup(1) call
    
    
    
}


/*ShowWordCount
 * 
 *  This method uses pipes to imitate the shell command: ls | grep p | wc
 *  used: pipe(), fork(), dup2() 
 */


void ShowWordCount()
{
 
    
    
    int pipe_fulld[2];
    
    pipe(pipe_fulld);
    
    pid_t pid = fork();
    
    
    
    if(pid == 0)
    {
        close(pipe_fulld[0]);
        
        printf("pid2");
        dup2(pipe_fulld[1] , 1);        
        execlp("ls","ls",NULL);
        perror("Error : Argument\n");
        exit(-1);
    }
    

    
    
    
    close(pipe_fulld[1]);
    
    wait(NULL);
    
    int pipe_fulld2[2];
    pipe(pipe_fulld2);
    pid_t pid2 = fork();
    
    if(pid2 == 0)
    {
       dup2(pipe_fulld[0],0);
       dup2(pipe_fulld2[1],1);
       
       execlp("grep","grep","p",NULL);
       perror("Error : Argumentss\n");
       exit(-1);
    }

    close(pipe_fulld2[1]);
    dup2(pipe_fulld2[0],0);
    
    execlp("wc","wc",NULL);
    
}


/*SigHandle
 * 
 *  This method handles the incoming signal while executing the method SigTest()
 */


void SigHandle(int sig)
{
    printf("Cannot kill this process with ctrl c :)\n");
}


/*SigTest
 * 
 *  This method demonstates the working of signals and how they can be handled
 *  used: signal() 
 */


void SigTest()
{
    signal(SIGINT,SigHandle);
    int i = 0;
    while(1)
    {
        printf("Demonstrating Signals..%d\n",++i);
        sleep(1);
    }
    
    
}
